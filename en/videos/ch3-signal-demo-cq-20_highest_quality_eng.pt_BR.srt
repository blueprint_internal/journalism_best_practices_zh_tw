﻿1
00:00:01,479 --> 00:00:05,136
Neste curto vídeo destacaremos as capacidades do Signal,

2
00:00:05,136 --> 00:00:10,019
uma ferramenta criada para jornalistas navegarem pelo conteúdo do Facebook e Instagram

3
00:00:10,019 --> 00:00:14,379
sobre novidades, entretenimento, esportes e outras categorias de conteúdo.

4
00:00:14,379 --> 00:00:17,377
No painel do Signal, você pode ver quais tópicos

5
00:00:17,377 --> 00:00:20,956
estão gerando a maioria das conversas em tempo real.

6
00:00:20,956 --> 00:00:24,254
Esses assuntos do momento aparecem em ordem de popularidade.

7
00:00:24,254 --> 00:00:27,531
Clique em qualquer tópico para ver publicações relacionadas

8
00:00:27,531 --> 00:00:30,664
por grau de cor e contexto.

9
00:00:30,664 --> 00:00:35,365
Você pode alterar para categorias para visualizar quais figuras públicas

10
00:00:35,365 --> 00:00:39,846
estão falando sobre os principais assuntos de política, negócios,

11
00:00:39,846 --> 00:00:44,087
entretenimento, esportes e outros domínios.

12
00:00:44,087 --> 00:00:47,634
Para saber quais tópicos estão começando a ser comentados no Facebook,

13
00:00:47,634 --> 00:00:50,684
clique na visualização emergente.

14
00:00:50,684 --> 00:00:54,169
Você também pode navegar pelo conteúdo do Facebook usando a pesquisa de palavra-chave.

15
00:00:58,089 --> 00:01:02,222
Os resultados retornados para você são apresentados em ordem cronológica invertida,

16
00:01:02,222 --> 00:01:04,967
com o mais recente no topo.

17
00:01:04,967 --> 00:01:07,679
Você também pode encontrar conteúdo visual no Instagram

18
00:01:07,679 --> 00:01:12,518
pesquisando hashtags, marcas de localização ou nomes de conta de usuário.

19
00:01:15,425 --> 00:01:20,323
Conforme você encontra publicações interessantes,

20
00:01:20,323 --> 00:01:23,806
é possível salvá-las nas suas coleções

21
00:01:23,806 --> 00:01:28,791
para uso posterior para integrar no seu artigo online ou página da web.

22
00:01:28,791 --> 00:01:30,872
Quando você estiver pronto para adicionar a publicação na sua página da web,

23
00:01:30,872 --> 00:01:37,180
basta pegar o código integrado e copiá-lo em seu próprio código.

24
00:01:37,180 --> 00:01:40,879
Há muito mais para ser feito com o Signal do que mostramos

25
00:01:40,879 --> 00:01:41,951
nesta rápida demonstração.

26
00:01:41,951 --> 00:01:45,049
Para saber mais, comece a usar o Signal hoje.

27
00:01:45,049 --> 00:01:53,217
Solicite acesso usando o link neste último capítulo do curso do Blueprint.

28
00:01:53,217 --> 00:01:57,918
E este é o fim desta curta demonstração do Signal.

